# /sprout-yy/book/filter-list.json

```json
{
  "level": ["js", "lv4", "lv5", "ss"],
  "unit": [
    "u1", "u2", "u3", "u4", "u5", "u6", "u7", "u8",
    "u9", "u10", "u11", "u12", "u13", "u14", "u15", "u16"
  ],
  "course": ["l1", "l2", "l3", "l4"],
  "difficulty": ["d1", "d2", "d3"],
  "teacher": ["tara", "gasper", "jennifer", "matt"]
}
```

# /sprout-yy/book/theme.json

```json
{
  "theme": ["orange", "green", "blue"]
}
```

# /sprout-yy/book/screen.json

```json
{
  "screen": ["大屏", "中屏", "小屏"]
}
```

# /sprout-yy/book/link-name.json

```json
{
  "linkName": [
    { "name": "动画播放", "key": "Animation Play", "segment": "no-reward" },
    { "name": "拼读/文本呈现", "key": "Blending/Text Presentation", "segment": "no-reward" },
    { "name": "拼读跟读", "key": "Blending Repetition", "segment": "reading-repetition" },
    { "name": "课堂奖励", "key": "Class reward", "segment": "no-reward" },
    { "name": "对话介绍", "key": "Dialog Introduction", "segment": "no-reward" },
    { "name": "课程结束", "key": "Ending", "segment": "no-reward" },
    { "name": "开场问候", "key": "Greeting", "segment": "no-reward" },
    { "name": "课程引入", "key": "Lead-in", "segment": "no-reward" },
    { "name": "听力练习", "key": "Listening Activity", "segment": "listening-activity" },
    { "name": "听力跟读", "key": "Listening Repetition", "segment": "listening-repetition" },
    { "name": "听力理解", "key": "Listening Comprehension", "segment": "listening-repetition" },
    { "name": "听力游戏+弱跟读", "key": "Listening Game +Repetition", "segment": "listening-game" },
    { "name": "图片展示+练习+弱跟读", "key": "Picture Walk + Interaction +Repetition", "segment": "listening-activity" },
    { "name": "巩固练习+弱跟读", "key": "Presentation Interaction + Repetition", "segment": "listening-activity" },
    { "name": "阅读练习", "key": "Reading Activity", "segment": "reading-activity" },
    { "name": "阅读理解", "key": "Reading Comprehension", "segment": "reading-repetition" },
    { "name": "词汇复习游戏", "key": "Review Game", "segment": "reading-game" },
    { "name": "角色扮演", "key": "Role-play", "segment": "speaking-game" },
    { "name": "句子排序", "key": "Sentence Builder", "segment": "writing-activity" },
    { "name": "歌曲表演", "key": "Song", "segment": "no-reward" },
    { "name": "歌曲表演", "key": "Song Performance", "segment": "no-reward" },
    { "name": "口语游戏", "key": "Speaking Game", "segment": "speaking-game" },
    { "name": "故事结局", "key": "Story Ending", "segment": "no-reward" },
    { "name": "教师呈现", "key": "Teacher Presentation", "segment": "no-reward" },
    { "name": "转场：一起学习吧!", "key": "Transition: Let's learn!", "segment": "no-reward" },
    { "name": "转场：一起来玩吧!", "key": "Transition: Let's play!", "segment": "no-reward" },
    { "name": "转场：一起练习吧!", "key": "Transition: Let's practice!", "segment": "no-reward" },
    { "name": "转场：一起来复习吧!", "key": "Transition: Let's review!", "segment": "no-reward" },
    { "name": "转场：再唱一次歌吧!", "key": "Transition: Let's sing again!", "segment": "no-reward" },
    { "name": "转场：一起唱歌吧!", "key": "Transition: Let's sing!", "segment": "no-reward" },
    { "name": "转场：一起来看个视频吧!", "key": "Transition: Let's watch a video!", "segment": "no-reward" },
    { "name": "转场：再看一遍视频吧!", "key": "Transition: Let's watch the video again!", "segment": "no-reward" },
    { "name": "转场：故事时间到!", "key": "Transition: Story time!", "segment": "no-reward" },
    { "name": "单词拼写", "key": "Word builder", "segment": "writing-activity" },
    { "name": "转场：一起聊聊吧!", "key": "Transition: Let's talk!", "segment": "no-reward" },
  ]
}
```

# /sprout-yy/book/skill.json

```json
{
  "presentationSet": "disable",
  "videoSet": "disable",
  "2aSet": [
    { "key": "rw", "label": "repeat word" },
    { "key": "sw", "label": "say word" },
    { "key": "rc", "label": "repeat chunk" },
    { "key": "rs", "label": "repeat sentence" },
    { "key": "ss", "label": "say sentence" }
  ],
  "2pSet": [
    { "key": "eng", "label": "english practice" },
    { "key": "cog", "label": "cognitive practice" }
  ],
  "gameSet": [
    { "key": "eng", "label": "english practice" },
    { "key": "cog", "label": "cognitive practice" }
  ],
  "freeSet": [
    { "key": "cs", "label": "choose speak" },
    { "key": "fs", "label": "free speak" },
    { "key": "eng", "label": "english practice" },
    { "key": "cog", "label": "cognitive practice" }
  ]
}
```

# /sprout-yy/book/big-question.json

```json
[
  {
    "name": "presentation_teaching",
    "type": "presentationSet",
    "supportMultiSmallQuestion": false,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "disable",
    "srEnd": "disable",
    "bigTr": "disable"
  },
  {
    "name": "presentation_award",
    "type": "presentationSet",
    "supportMultiSmallQuestion": false,
    "screen": "fromPreviousSmallQuestion",
    "background": "show",
    "video": "disable",
    "srStart": "disable",
    "srEnd": "disable",
    "bigTr": "disable"
  },
  {
    "name": "presentation_exchange",
    "type": "presentationSet",
    "supportMultiSmallQuestion": false,
    "screen": "disableBigScreen",
    "background": "show",
    "video": "disable",
    "srStart": "disable",
    "srEnd": "disable",
    "bigTr": "disable"
  },
  {
    "name": "play_video",
    "type": "videoSet",
    "supportMultiSmallQuestion": false,
    "screen": "show",
    "background": "show",
    "video": [
      "start video",
      "routine-song",
      "theme-song",
      "convo-video",
      "Leo Press Button"
    ],
    "srStart": "disable",
    "srEnd": "disable",
    "bigTr": "disable"
  },
  {
    "name": "a2a",
    "type": "2aSet",
    "supportMultiSmallQuestion": false,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "pa2a_trigger",
    "type": "2aSet",
    "supportMultiSmallQuestion": false,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "pa2a_build1",
    "type": "2aSet",
    "supportMultiSmallQuestion": true,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "pa2a_build2",
    "type": "2aSet",
    "supportMultiSmallQuestion": true,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "a2p_tap1",
    "type": "2pSet",
    "supportMultiSmallQuestion": true,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "a2p_tap2",
    "type": "2pSet",
    "supportMultiSmallQuestion": true,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "a2p_drag1",
    "type": "2pSet",
    "supportMultiSmallQuestion": false,
    "screen": "show",
    "background": "disable",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "a2p_drag2",
    "type": "2pSet",
    "supportMultiSmallQuestion": true,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "a2p_drag3",
    "type": "2pSet",
    "supportMultiSmallQuestion": true,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "a2p_tool",
    "type": "2pSet",
    "supportMultiSmallQuestion": false,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "a2p_reveal",
    "type": "2pSet",
    "supportMultiSmallQuestion": false,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "a2p_puzzle",
    "type": "2pSet",
    "supportMultiSmallQuestion": false,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "a2a_free",
    "type": "freeSet",
    "supportMultiSmallQuestion": false,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "pa2a_free",
    "type": "freeSet",
    "supportMultiSmallQuestion": false,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "a2p_free",
    "type": "freeSet",
    "supportMultiSmallQuestion": false,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "show",
    "srEnd": "show",
    "bigTr": "show"
  },
  {
    "name": "pa2a_jet1",
    "type": "gameSet",
    "supportMultiSmallQuestion": true,
    "screen": "show",
    "background": "disable",
    "video": "disable",
    "srStart": "disable",
    "srEnd": "disable",
    "bigTr": "disable"
  },
  {
    "name": "pa2a_jet2",
    "type": "gameSet",
    "supportMultiSmallQuestion": true,
    "screen": "show",
    "background": "disable",
    "video": "disable",
    "srStart": "disable",
    "srEnd": "disable",
    "bigTr": "disable"
  },
  {
    "name": "pa2a_roleplay1",
    "type": "gameSet",
    "supportMultiSmallQuestion": true,
    "screen": "show",
    "background": "disable",
    "video": "disable",
    "srStart": "disable",
    "srEnd": "disable",
    "bigTr": "disable"
  },
  {
    "name": "pa2a_roleplay2",
    "type": "gameSet",
    "supportMultiSmallQuestion": true,
    "screen": "show",
    "background": "disable",
    "video": "disable",
    "srStart": "disable",
    "srEnd": "disable",
    "bigTr": "disable"
  },
  {
    "name": "a2p_fall",
    "type": "gameSet",
    "supportMultiSmallQuestion": true,
    "screen": "show",
    "background": "show",
    "video": "disable",
    "srStart": "disable",
    "srEnd": "disable",
    "bigTr": "disable"
  },
  {
    "name": "a2p_mole",
    "type": "gameSet",
    "supportMultiSmallQuestion": true,
    "screen": "show",
    "background": "disable",
    "video": "disable",
    "srStart": "disable",
    "srEnd": "disable",
    "bigTr": "disable"
  },
  {
    "name": "t2p_mole",
    "type": "gameSet",
    "supportMultiSmallQuestion": true,
    "screen": "show",
    "background": "disable",
    "video": "disable",
    "srStart": "disable",
    "srEnd": "disable",
    "bigTr": "disable"
  }
]
```

# /sprout-yy/book/small-question.json

```json
[
  {
    "name": "presentation_teaching",
    "stem": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "presentation_award",
    "stem": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "presentation_exchange",
    "stem": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "play_video"
  },
  {
    "name": "a2a",
    "stem": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": {
        "placeholder": "支持多个变种",
        "required": 1,
        "optional": 0
      },
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "pa2a_trigger",
    "stem": {
      "imgStart": {
        "placeholder": "",
        "required": 1,
        "optional": 0
      },
      "imgEnd": {
        "placeholder": "读错",
        "required": 1,
        "optional": 0
      },
      "videoEnd": {
        "placeholder": "读对",
        "required": 1,
        "optional": 0
      },
      "text": "disable",
      "keywords": {
        "placeholder": "支持多个变种",
        "required": 1,
        "optional": 0
      },
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "pa2a_build1",
    "stem": {
      "imgStart": {
        "placeholder": "",
        "required": 1,
        "optional": 0
      },
      "imgEnd": {
        "placeholder": "读对",
        "required": 0,
        "optional": 1
      },
      "videoEnd": "disable",
      "text": "disable",
      "keywords": {
        "placeholder": "支持多个变种",
        "required": 1,
        "optional": 0
      },
      "offset": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      },
      "disappear": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      }
    },
    "right": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "pa2a_build2",
    "stem": {
      "imgStart": {
        "placeholder": "",
        "required": 1,
        "optional": 0
      },
      "imgEnd": {
        "placeholder": "读对",
        "required": 0,
        "optional": 1
      },
      "videoEnd": "disable",
      "text": "disable",
      "keywords": {
        "placeholder": "支持多个变种",
        "required": 1,
        "optional": 0
      },
      "offset": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      },
      "disappear": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      }
    },
    "right": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "a2p_tap1",
    "stem": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": {
        "placeholder": "",
        "required": 1,
        "optional": 5
      },
      "imgEnd": {
        "placeholder": "选对",
        "required": 0,
        "optional": 6
      },
      "text": "disable",
      "offset": {
        "placeholder": "选对",
        "required": 0,
        "optional": 6
      },
      "disappear": {
        "placeholder": "选对",
        "required": 0,
        "optional": 6
      }
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      }
    }
  },
  {
    "name": "a2p_tap2",
    "stem": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": {
        "placeholder": "",
        "required": 1,
        "optional": 5
      },
      "imgEnd": {
        "placeholder": "选对",
        "required": 0,
        "optional": 6
      },
      "text": "disable",
      "offset": {
        "placeholder": "选对",
        "required": 0,
        "optional": 6
      },
      "disappear": {
        "placeholder": "选对",
        "required": 0,
        "optional": 6
      }
    },
    "wrong": {
      "imgStart": {
        "placeholder": "选对",
        "required": 0,
        "optional": 4
      },
      "text": "disable"
    },
    "tr": {
      "img": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      }
    }
  },
  {
    "name": "a2p_drag1",
    "stem": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": {
        "placeholder": "",
        "required": 1,
        "optional": 5
      },
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": {
        "placeholder": "",
        "required": 1,
        "optional": 3
      },
      "text": "disable"
    },
    "tr": {
      "img": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      }
    }
  },
  {
    "name": "a2p_drag2",
    "stem": {
      "imgStart": {
        "placeholder": "",
        "required": 0,
        "optional": 6
      },
      "imgEnd": {
        "placeholder": "选对",
        "required": 0,
        "optional": 6
      },
      "videoEnd": "disable",
      "text": "disable",
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": {
        "placeholder": "",
        "required": 1,
        "optional": 5
      },
      "imgEnd": {
        "placeholder": "选对",
        "required": 0,
        "optional": 6
      },
      "text": "disable",
      "offset": {
        "placeholder": "",
        "required": 1,
        "optional": 5
      },
      "disappear": {
        "placeholder": "选对",
        "required": 0,
        "optional": 6
      }
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      }
    }
  },
  {
    "name": "a2p_drag3",
    "stem": {
      "imgStart": {
        "placeholder": "",
        "required": 0,
        "optional": 6
      },
      "imgEnd": {
        "placeholder": "选对",
        "required": 0,
        "optional": 6
      },
      "videoEnd": "disable",
      "text": "disable",
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": {
        "placeholder": "",
        "required": 1,
        "optional": 5
      },
      "imgEnd": {
        "placeholder": "选对",
        "required": 0,
        "optional": 6
      },
      "text": "disable",
      "offset": {
        "placeholder": "",
        "required": 1,
        "optional": 5
      },
      "disappear": {
        "placeholder": "选对",
        "required": 0,
        "optional": 6
      }
    },
    "wrong": {
      "imgStart": {
        "placeholder": "",
        "required": 0,
        "optional": 4
      },
      "text": "disable"
    },
    "tr": {
      "img": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      }
    }
  },
  {
    "name": "a2p_tool",
    "stem": {
      "imgStart": {
        "placeholder": "",
        "required": 1,
        "optional": 0
      },
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": {
        "placeholder": "",
        "required": 1,
        "optional": 5
      },
      "imgEnd": {
        "placeholder": "选对",
        "required": 0,
        "optional": 6
      },
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": {
        "placeholder": "",
        "required": 0,
        "optional": 4
      },
      "text": "disable"
    },
    "tr": {
      "img": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      }
    }
  },
  {
    "name": "a2p_reveal",
    "stem": {
      "imgStart": {
        "placeholder": "下面的图",
        "required": 1,
        "optional": 5
      },
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": {
        "placeholder": "上面的遮盖物",
        "required": 1,
        "optional": 5
      },
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      }
    }
  },
  {
    "name": "a2p_puzzle",
    "stem": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": {
        "placeholder": "",
        "required": 2,
        "optional": 6
      },
      "imgEnd": {
        "placeholder": "选对",
        "required": 2,
        "optional": 6
      },
      "text": "disable",
      "offset": {
        "placeholder": "",
        "required": 2,
        "optional": 6
      },
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      }
    }
  },
  {
    "name": "a2a_free",
    "stem": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": {
        "placeholder": "支持多个变种",
        "required": 2,
        "optional": 3
      },
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "pa2a_free",
    "stem": {
      "imgStart": {
        "placeholder": "",
        "required": 2,
        "optional": 3
      },
      "imgEnd": {
        "placeholder": "读对",
        "required": 2,
        "optional": 3
      },
      "videoEnd": "disable",
      "text": "disable",
      "keywords": {
        "placeholder": "支持多个变种",
        "required": 2,
        "optional": 3
      },
      "offset": {
        "placeholder": "",
        "required": 2,
        "optional": 3
      },
      "disappear": "disable"
    },
    "right": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "a2p_free",
    "stem": {
      "imgStart": {
        "placeholder": "工具",
        "required": 0,
        "optional": 1
      },
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": {
        "placeholder": "",
        "required": 2,
        "optional": 3
      },
      "imgEnd": {
        "placeholder": "选对",
        "required": 2,
        "optional": 3
      },
      "text": "disable",
      "offset": {
        "placeholder": "",
        "required": 0,
        "optional": 6
      },
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      }
    }
  },
  {
    "name": "pa2a_jet1",
    "stem": {
      "imgStart": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      },
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      },
      "keywords": {
        "placeholder": "",
        "required": 1,
        "optional": 0
      },
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "pa2a_jet2",
    "stem": {
      "imgStart": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      },
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      },
      "keywords": {
        "placeholder": "",
        "required": 1,
        "optional": 0
      },
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "pa2a_roleplay1",
    "stem": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": {
        "placeholder": "",
        "required": 1,
        "optional": 0
      },
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "pa2a_roleplay2",
    "stem": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": {
        "placeholder": "",
        "required": 1,
        "optional": 0
      },
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": "disable",
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "a2p_fall",
    "stem": {
      "imgStart": {
        "placeholder": "篮子",
        "required": 1,
        "optional": 0
      },
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      },
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": {
        "placeholder": "",
        "required": 1,
        "optional": 5
      },
      "imgEnd": "disable",
      "text": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": {
        "placeholder": "",
        "required": 0,
        "optional": 4
      },
      "text": "disable"
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "a2p_mole",
    "stem": {
      "imgStart": "disable",
      "imgEnd": "disable",
      "videoEnd": "disable",
      "text": "disable",
      "keywords": "disable",
      "offset": "disable",
      "disappear": "disable"
    },
    "right": {
      "imgStart": {
        "placeholder": "",
        "required": 0,
        "optional": 6
      },
      "imgEnd": "disable",
      "text": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      },
      "offset": "disable",
      "disappear": "disable"
    },
    "wrong": {
      "imgStart": {
        "placeholder": "",
        "required": 0,
        "optional": 4
      },
      "text": {
        "placeholder": "",
        "required": 0,
        "optional": 1
      }
    },
    "tr": {
      "img": "disable"
    }
  },
  {
    "name": "t2p_mole"
  }
]
```

# /sprout-yy/book/artboard.json

```json
[
  {
    "name": "presentation_teaching",
    "bigQuestionStart": true,
    "bigQuestionEnd": false,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": false,
    "needRename": false
  },
  {
    "name": "presentation_award",
    "bigQuestionStart": true,
    "bigQuestionEnd": false,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": false,
    "needRename": false
  },
  {
    "name": "presentation_exchange",
    "bigQuestionStart": true,
    "bigQuestionEnd": false,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": false,
    "needRename": false
  },
  {
    "name": "play_video",
    "bigQuestionStart": true,
    "bigQuestionEnd": false,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": false,
    "needRename": false
  },
  {
    "name": "a2a",
    "bigQuestionStart": false,
    "bigQuestionEnd": false,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": false,
    "needRename": false
  },
  {
    "name": "pa2a_trigger",
    "bigQuestionStart": true,
    "bigQuestionEnd": false,
    "smallQuestionStart": false,
    "smallQuestionEnd": true,
    "trCard": false,
    "needRename": false
  },
  {
    "name": "pa2a_build1",
    "bigQuestionStart": true,
    "bigQuestionEnd": true,
    "smallQuestionStart": false,
    "smallQuestionEnd": true,
    "trCard": false,
    "needRename": true
  },
  {
    "name": "pa2a_build2",
    "bigQuestionStart": false,
    "bigQuestionEnd": true,
    "smallQuestionStart": true,
    "smallQuestionEnd": true,
    "trCard": false,
    "needRename": true
  },
  {
    "name": "a2p_tap1",
    "bigQuestionStart": true,
    "bigQuestionEnd": true,
    "smallQuestionStart": false,
    "smallQuestionEnd": true,
    "trCard": true,
    "needRename": true
  },
  {
    "name": "a2p_tap2",
    "bigQuestionStart": false,
    "bigQuestionEnd": true,
    "smallQuestionStart": true,
    "smallQuestionEnd": true,
    "trCard": true,
    "needRename": true
  },
  {
    "name": "a2p_drag1",
    "bigQuestionStart": false,
    "bigQuestionEnd": false,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": true,
    "needRename": false
  },
  {
    "name": "a2p_drag2",
    "bigQuestionStart": true,
    "bigQuestionEnd": true,
    "smallQuestionStart": false,
    "smallQuestionEnd": true,
    "trCard": true,
    "needRename": true
  },
  {
    "name": "a2p_drag3",
    "bigQuestionStart": false,
    "bigQuestionEnd": true,
    "smallQuestionStart": true,
    "smallQuestionEnd": true,
    "trCard": true,
    "needRename": true
  },
  {
    "name": "a2p_tool",
    "bigQuestionStart": true,
    "bigQuestionEnd": true,
    "smallQuestionStart": false,
    "smallQuestionEnd": true,
    "trCard": true,
    "needRename": true
  },
  {
    "name": "a2p_reveal",
    "bigQuestionStart": true,
    "bigQuestionEnd": true,
    "smallQuestionStart": false,
    "smallQuestionEnd": true,
    "trCard": true,
    "needRename": true
  },
  {
    "name": "a2p_puzzle",
    "bigQuestionStart": true,
    "bigQuestionEnd": true,
    "smallQuestionStart": false,
    "smallQuestionEnd": true,
    "trCard": true,
    "needRename": true
  },
  {
    "name": "a2a_free",
    "bigQuestionStart": true,
    "bigQuestionEnd": false,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": false,
    "needRename": false
  },
  {
    "name": "pa2a_free",
    "bigQuestionStart": true,
    "bigQuestionEnd": true,
    "smallQuestionStart": false,
    "smallQuestionEnd": true,
    "trCard": false,
    "needRename": true
  },
  {
    "name": "a2p_free",
    "bigQuestionStart": true,
    "bigQuestionEnd": true,
    "smallQuestionStart": false,
    "smallQuestionEnd": true,
    "trCard": true,
    "needRename": true
  },
  {
    "name": "pa2a_jet1",
    "bigQuestionStart": false,
    "bigQuestionEnd": false,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": false,
    "needRename": false
  },
  {
    "name": "pa2a_jet2",
    "bigQuestionStart": false,
    "bigQuestionEnd": false,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": false,
    "needRename": false
  },
  {
    "name": "pa2a_roleplay1",
    "bigQuestionStart": true,
    "bigQuestionEnd": true,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": false,
    "needRename": true
  },
  {
    "name": "pa2a_roleplay2",
    "bigQuestionStart": true,
    "bigQuestionEnd": true,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": false,
    "needRename": true
  },
  {
    "name": "a2p_fall",
    "bigQuestionStart": true,
    "bigQuestionEnd": false,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": false,
    "needRename": false
  },
  {
    "name": "a2p_mole",
    "bigQuestionStart": false,
    "bigQuestionEnd": false,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": false,
    "needRename": false
  },
  {
    "name": "t2p_mole",
    "bigQuestionStart": false,
    "bigQuestionEnd": false,
    "smallQuestionStart": false,
    "smallQuestionEnd": false,
    "trCard": false,
    "needRename": false
  }
]
```

# /sprout-yy/book/video-clip.json

```json
[
  {
    "name": "presentation_teaching",
    "videoClip": [
      {
        "name": "non-interactive",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "presentation_award",
    "videoClip": [
      {
        "name": "non-interactive",
        "lines": "required",
        "actions": {
          "placeholder": "hold sticker, sticker disappear and fly out of the screen",
          "type": "disabled"
        },
        "stage": {
          "placeholder": "sticker",
          "type": "disabled"
        },
        "sound": "required",
        "special": {
          "placeholder": "sticker 从老师手中消失",
          "type": "disabled"
        }
      }
    ]
  },
  {
    "name": "presentation_exchange",
    "videoClip": [
      {
        "name": "non-interactive",
        "lines": "required",
        "actions": {
          "placeholder": "hold \"treasure box\" for a while.",
          "type": "disabled"
        },
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "play_video"
  },
  {
    "name": "a2a",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "19s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr_1",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "prompt_2",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "waiting_2",
        "lines": {
          "placeholder": "10s",
          "type": "disabled"
        },
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "ok_tr_2",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "prompt_3",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "waiting_3",
        "lines": {
          "placeholder": "10s",
          "type": "disabled"
        },
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "pa2a_trigger",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "23s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr_1",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "prompt_2",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "waiting_2",
        "lines": {
          "placeholder": "10s",
          "type": "disabled"
        },
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "ok_tr_2",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "prompt_3",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "waiting_3",
        "lines": {
          "placeholder": "10s",
          "type": "disabled"
        },
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "pa2a_build1",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "49s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr_1",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "prompt_2",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "waiting_2",
        "lines": {
          "placeholder": "10s",
          "type": "disabled"
        },
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "ok_tr_2",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "prompt_3",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "waiting_3",
        "lines": {
          "placeholder": "10s",
          "type": "disabled"
        },
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "pa2a_build2",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "49s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr_1",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "prompt_2",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "waiting_2",
        "lines": {
          "placeholder": "10s",
          "type": "disabled"
        },
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "ok_tr_2",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "prompt_3",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "waiting_3",
        "lines": {
          "placeholder": "10s",
          "type": "disabled"
        },
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "a2p_tap1",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "49s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "a2p_tap2",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "43s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "a2p_drag1",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "32s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "a2p_drag2",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "69s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "a2p_drag3",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "61s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "a2p_tool",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "118s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "a2p_reveal",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "13.5s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "a2p_puzzle",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "72s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "a2a_free",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "12s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "tr_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "tr_2",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "tr_3",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "tr_4",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "tr_5",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      }
    ]
  },
  {
    "name": "pa2a_free",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "41.5s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "tr_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "tr_2",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "tr_3",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "tr_4",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "tr_5",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      }
    ]
  },
  {
    "name": "a2p_free",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "30s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "tr_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "tr_2",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "tr_3",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "tr_4",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      },
      {
        "name": "tr_5",
        "lines": "optional",
        "actions": "optional",
        "stage": "optional",
        "sound": "optional",
        "special": "optional"
      }
    ]
  },
  {
    "name": "pa2a_jet1",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "267s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "pa2a_jet2",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "267s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "pa2a_roleplay1",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "88s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "pa2a_roleplay2",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "88s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "a2p_fall",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "65s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "a2p_mole",
    "videoClip": [
      {
        "name": "prompt_1",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "waiting_1",
        "lines": {
          "placeholder": "150s",
          "type": "disabled"
        },
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "good_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "ok_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      },
      {
        "name": "bad_tr",
        "lines": "required",
        "actions": "required",
        "stage": "required",
        "sound": "required",
        "special": "required"
      }
    ]
  },
  {
    "name": "t2p_mole"
  }
]
```

# /sprout-yy/book/creation-record.json

```json
{
  "record": [
    {
      "id": "123456789",
      "level": "js",
      "unit": "u1",
      "course": "l1",
      "difficulty": "d1",
      "teacher": "tara",
      "data": "https://cdn-link/book/versions/123456789.json"
    }
  ]
}
```

# [cdn] https://cdn-link/book/versions/123456789.json

```json
{
  "versions": [
    {
      "version": "v1.2",
      "createdAt": "123456789",
      "author": "guang.xing",
      "deleted": false,
      "scriptStatus": false,
      "storyStatus": false,
      "nameStatus": false,
      "data": "https://cdn-link/book/detail/123456789.json"
    }
  ]
}
```

# [cdn] https://cdn-link/book/detail/123456789.json

```json
{
  "theme": "orange",
  "background": {
    "keywords": "xxx",
    "description": "xxx",
    "image": ["xxx"]
  },
  "award": {
    "keywords": "xxx",
    "description": "xxx",
    "image": ["xxx"]
  },
  "sections": [
    {
      "number": "01",
      "order": 1,
      "name": "xxxx",
      "bigQuestions": [
        {
          "nunmber": "01",
          "order": 1,
          "name": "xxx",
          "skill": "xxx",
          "screen": "xxx",
          "background": "xxx",
          "srStart": "xxx",
          "srEnd": "xxx",
          "bigTr": "xxx",
          "video": "xxx",
          "startImage": {
            "image": ["xxx"],
            "keywords": "xxx"
          },
          "endImage": {
            "image": ["xxx"],
            "keywords": "xxx"
          },
          "smallQuestions": [
            {
              "startImage": {
                "image": ["xxx"],
                "keywords": "xxx"
              },
              "endImage": {
                "image": ["xxx"],
                "keywords": "xxx"
              },
              "trImage": {
                "image": ["xxx"],
                "keywords": "xxx"
              },
              "stem": {
                "parts": [
                  {
                    "start": "xxx",
                    "end": "xxx",
                    "keywords": "xxx",
                    "offset": "xxx",
                    "disappear": "xxx",
                  }
                ],
                "video": "xxx",
                "text": "xxx"
              },
              "right": {
                "parts": [
                  {
                    "start": "xxx",
                    "end": "xxx",
                    "offset": "xxx",
                    "disappear": "xxx",
                  }
                ],
                "text": "xxx"
              },
              "wrong": {
                "parts": [
                  {
                    "start": "xxx",
                  }
                ],
                "text": "xxx"
              },
              "tr": {
                "image": ["xxx"],
                "description": "xxx"
              },
              "videos": [
                {
                  "name": "xxx",
                  "screen": "大屏",
                  "tts": "xxx",
                  "lines": "xxx",
                  "actions": "xxx",
                  "stage": "xxx",
                  "sound": "xxx",
                  "special": "xxx"
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}
```