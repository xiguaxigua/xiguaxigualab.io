function Promise (executor) {
  console.log('==in Promise constructor')
  let self = this
  
  self.status = 'pending'
  self.value = undefined

  self.onResolvedCallBacks = []
  self.onRejectedCallBacks = []

  function resolve (value) {
    console.log('==promise>resolve', value)
    if (value instanceof Promise) return value.then(resolve, reject)

    setTimeout(function () {
      if (self.status === 'pending') {
        self.status = 'fulfilled'
        self.value = value
        self.onResolvedCallBacks.forEach(item => item(self.value))
        console.log('==promise>resolve async', self)
      }
    }, 0)
  }

  function reject (reason) {
    console.log('==promise>reject', reason)
    setTimeout(function () {
      if (self.status === 'pending') {
        self.status = 'rejected'
        self.value = reason
        self.onRejectedCallBacks.forEach(item => item(self.value))
        console.log('==promise>reject async', self)
      }
    }, 0)
  }

  try {
    executor(resolve, reject)
  } catch (e) {
    reject(e)
  }
}

Promise.prototype.then = function (onFulfilled, onRejected) {
  onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : value => value
  onRejected = typeof onRejected === 'function' ? onRejected : value => value
  
  let self = this
  let promise2
  console.log('==enter then function', self)
  if (self.status === 'fulfilled') {
    promise2 = new Promise(function (resolve, reject) {
      setTimeout(function () {
        try {
          let x = onFulfilled(self.value)
          resolvePromise(promise2, x, resolve, reject)
        } catch (e) {
          reject(e)
        }
      }, 0)
    })
  }

  if (self.status === 'rejected') {
    promise2 = new Promise(function (resolve, reject) {
      setTimeout(function () {
        try {
          let x = onRejected(self.value)
          resolvePromise(promise2, x, resolve, reject)
        } catch (e) {
          reject(e)
        }
      })
    })
  }

  if (self.status === 'pending') {
    promise2 = new Promise(function (resolve, reject) {
      
      self.onResolvedCallBacks.push(function () {
        try {
          let x = onFulfilled(self.value)
          resolvePromise(promise2, x, resolve, reject)
        } catch (e) {
          reject(e)
        }
      })

      self.onRejectedCallBacks.push(function () {
        try {
          let x = onRejected(self.value)
          resolvePromise(promise2, x, resolve, reject)
        } catch (e) {
          reject(e)
        }
      })

      console.log('==length', self.onResolvedCallBacks.length)
      console.log('==length', self.onRejectedCallBacks.length)
    })
  }
  console.log('==propmise2', promise2)

  return promise2
}

function resolvePromise (promise2, x, resolve, reject) {
  if (promise2 === x) return reject(new TypeError('循环引用'))
  let then, called
  if (x != null && (typeof x === 'function' || typeof x === 'object')) {
    try {
      then = x.then
      if (typeof then === 'function') {
        then.call(x, function (data) {
          if (called) return
          called = true
          resolvePromise(promise2, data, resolve, reject)
        }, function (err) {
          if (called) return
          called = true
          reject(err)
        })
      } else {
        resolve(x)
      }
    } catch (e) {
      if (called) return
      called = true
      reject(e)
    }
  } else {
    resolve(x)
  }
}

Promise.prototype.catch = function (onRejected) {
  return this.then(null, onRejected)
}

var p = new Promise((resolve, reject) => {
  console.log('==run exector')
  setTimeout(() => {
    console.log('==run resolve')
    resolve(1)
  }, 10000)
})

console.log(1, p)

p.then(res => {
  console.log(res)
}, err => {
  console.log(err)
}).then(res => {
  console.log(res)
}, err => {
  console.log(err)
}).then(res => {
  console.log(res)
}, err => {
  console.log(err)
})

console.log(2, p)
