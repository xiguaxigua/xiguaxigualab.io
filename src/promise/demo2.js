function MyPromise (fn) {
  let status = 'pending'
  let value = null
  let callbacks = []

  this.then = (onFulfilled) => {
    return new MyPromise((resolve, reject) => {
      handle({ onFulfilled, resolve })
    })
  }

  function handle (callback) {
    if (status === 'pending') {
      callbacks.push(callback)
    }

    if (status === 'fulfilled') {
      if (!callback.onFulfilled) {
        callback.resolve(value)
        return
      }

      const res = callback.onFulfilled(value)
      return callback.resolve(res)
    }
  }

  function resolve (newValue) {
    const fn = () => {
      if (status !== 'pending') return
      status = 'fulfilled'
      value = newValue
      handleCb()
    }

    setTimeout(fn, 0)
  }

  function handleCb () {
    while (callbacks.length) {
      const cb = callbacks.shift()
      handle(cb)
    }
  }

  fn(resolve)
}

new MyPromise((resolve, reject) => {
  setTimeout(() => {
    resolve(1)
  }, 1000)
}).then(res => {
  console.log('resolve', res)
})