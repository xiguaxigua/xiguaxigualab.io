function MyPromise (fn) {
  let state = 'pending'
  let value = null
  const callbacks = []

  this.then = function (onFulfilled) {
    return new MyPromise((resolve, reject) => {
      handle({ onFulfilled, resolve })
    })
  }

  function resolve (newValue) {
    const fn1 = () => {
      if (state !== 'pending') return
      state = 'fulfilled'
      value = newValue
      handleCb()
    }

    setTimeout(fn1, 0)
  }

  function handleCb () {
    while (callbacks.length) {
      const fulfilledFn = callbacks.shift()
      handle(fulfilledFn)
    }
  }

  function handle (callback) {
    if (state === 'pending') {
      callbacks.push(callback)
      return
    }

    if (state === 'fulfilled') {
      if (!callback.onFulfilled) {
        callback.resolve(value)
        return
      }
      const res = callback.onFulfilled(value)
      callback.resolve(res)
    }
  }

  fn(resolve)
}

new MyPromise((resolve, reject) => {
  setTimeout(() => {
    resolve(1)
  }, 1000)
}).then(res => {
  console.log('then1', res)
})