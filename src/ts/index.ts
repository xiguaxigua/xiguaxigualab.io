/**
 * 相加操作
 * @param a 加数 a
 * @param b 加数 b
 * @returns number
 */
function add (a: number, b: number) {
  return a + b
}

/** props类型 */
interface Props {
  /** 类名 */
  className: string
}

const a: Props = {
  className: ''
}