var questions = {
  "questions": [
    {
      "name": "presentation_teaching",
      "type": "presentationSet",
      "supportMultiSmallQuestion": false,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "disable",
      "srEnd": "disable",
      "bigTr": "disable",
      "smallQuestion": {
        "stem": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "non-interactive",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": false,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": false,
        "needRename": false
      }
    },
    {
      "name": "presentation_award",
      "type": "presentationSet",
      "supportMultiSmallQuestion": false,
      "screen": "fromPreviousSmallQuestion",
      "background": "show",
      "video": "disable",
      "srStart": "disable",
      "srEnd": "disable",
      "bigTr": "disable",
      "smallQuestion": {
        "stem": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "non-interactive",
            "lines": "required",
            "actions": {
              "placeholder": "hold sticker, sticker disappear and fly out of the screen",
              "type": "disabled"
            },
            "stage": {
              "placeholder": "sticker",
              "type": "disabled"
            },
            "sound": "required",
            "special": {
              "placeholder": "sticker 从老师手中消失",
              "type": "disabled"
            }
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": false,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": false,
        "needRename": false
      }
    },
    {
      "name": "presentation_exchange",
      "type": "presentationSet",
      "supportMultiSmallQuestion": false,
      "screen": "disableBigScreen",
      "background": "show",
      "video": "disable",
      "srStart": "disable",
      "srEnd": "disable",
      "bigTr": "disable",
      "smallQuestion": {
        "stem": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "non-interactive",
            "lines": "required",
            "actions": {
              "placeholder": "hold \"treasure box\" for a while.",
              "type": "disabled"
            },
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": false,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": false,
        "needRename": false
      }
    },
    {
      "name": "play_video",
      "type": "videoSet",
      "supportMultiSmallQuestion": false,
      "screen": "show",
      "background": "show",
      "video": [
        "start video",
        "routine-song",
        "theme-song",
        "convo-video",
        "Leo Press Button"
      ],
      "srStart": "disable",
      "srEnd": "disable",
      "bigTr": "disable",
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": false,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": false,
        "needRename": false
      }
    },
    {
      "name": "a2a",
      "type": "2aSet",
      "supportMultiSmallQuestion": false,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": {
            "placeholder": "支持多个变种",
            "required": 1,
            "optional": 0
          },
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "19s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr_1",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "prompt_2",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "waiting_2",
            "lines": {
              "placeholder": "10s",
              "type": "disabled"
            },
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "ok_tr_2",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "prompt_3",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "waiting_3",
            "lines": {
              "placeholder": "10s",
              "type": "disabled"
            },
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": false,
        "bigQuestionEnd": false,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": false,
        "needRename": false
      }
    },
    {
      "name": "pa2a_trigger",
      "type": "2aSet",
      "supportMultiSmallQuestion": false,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": {
            "placeholder": "",
            "required": 1,
            "optional": 0
          },
          "imgEnd": {
            "placeholder": "读错",
            "required": 1,
            "optional": 0
          },
          "videoEnd": {
            "placeholder": "读对",
            "required": 1,
            "optional": 0
          },
          "text": "disable",
          "keywords": {
            "placeholder": "支持多个变种",
            "required": 1,
            "optional": 0
          },
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "23s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr_1",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "prompt_2",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "waiting_2",
            "lines": {
              "placeholder": "10s",
              "type": "disabled"
            },
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "ok_tr_2",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "prompt_3",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "waiting_3",
            "lines": {
              "placeholder": "10s",
              "type": "disabled"
            },
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": false,
        "smallQuestionStart": false,
        "smallQuestionEnd": true,
        "trCard": false,
        "needRename": false
      }
    },
    {
      "name": "pa2a_build1",
      "type": "2aSet",
      "supportMultiSmallQuestion": true,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": {
            "placeholder": "",
            "required": 1,
            "optional": 0
          },
          "imgEnd": {
            "placeholder": "读对",
            "required": 0,
            "optional": 1
          },
          "videoEnd": "disable",
          "text": "disable",
          "keywords": {
            "placeholder": "支持多个变种",
            "required": 1,
            "optional": 0
          },
          "offset": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          },
          "disappear": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          }
        },
        "right": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "49s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr_1",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "prompt_2",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "waiting_2",
            "lines": {
              "placeholder": "10s",
              "type": "disabled"
            },
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "ok_tr_2",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "prompt_3",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "waiting_3",
            "lines": {
              "placeholder": "10s",
              "type": "disabled"
            },
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": true,
        "smallQuestionStart": false,
        "smallQuestionEnd": true,
        "trCard": false,
        "needRename": true
      }
    },
    {
      "name": "pa2a_build2",
      "type": "2aSet",
      "supportMultiSmallQuestion": true,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": {
            "placeholder": "",
            "required": 1,
            "optional": 0
          },
          "imgEnd": {
            "placeholder": "读对",
            "required": 0,
            "optional": 1
          },
          "videoEnd": "disable",
          "text": "disable",
          "keywords": {
            "placeholder": "支持多个变种",
            "required": 1,
            "optional": 0
          },
          "offset": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          },
          "disappear": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          }
        },
        "right": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "49s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr_1",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "prompt_2",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "waiting_2",
            "lines": {
              "placeholder": "10s",
              "type": "disabled"
            },
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "ok_tr_2",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "prompt_3",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "waiting_3",
            "lines": {
              "placeholder": "10s",
              "type": "disabled"
            },
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": false,
        "bigQuestionEnd": true,
        "smallQuestionStart": true,
        "smallQuestionEnd": true,
        "trCard": false,
        "needRename": true
      }
    },
    {
      "name": "a2p_tap1",
      "type": "2pSet",
      "supportMultiSmallQuestion": true,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": {
            "placeholder": "",
            "required": 1,
            "optional": 5
          },
          "imgEnd": {
            "placeholder": "选对",
            "required": 0,
            "optional": 6
          },
          "text": "disable",
          "offset": {
            "placeholder": "选对",
            "required": 0,
            "optional": 6
          },
          "disappear": {
            "placeholder": "选对",
            "required": 0,
            "optional": 6
          }
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          }
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "49s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": true,
        "smallQuestionStart": false,
        "smallQuestionEnd": true,
        "trCard": true,
        "needRename": true
      }
    },
    {
      "name": "a2p_tap2",
      "type": "2pSet",
      "supportMultiSmallQuestion": true,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": {
            "placeholder": "",
            "required": 1,
            "optional": 5
          },
          "imgEnd": {
            "placeholder": "选对",
            "required": 0,
            "optional": 6
          },
          "text": "disable",
          "offset": {
            "placeholder": "选对",
            "required": 0,
            "optional": 6
          },
          "disappear": {
            "placeholder": "选对",
            "required": 0,
            "optional": 6
          }
        },
        "wrong": {
          "imgStart": {
            "placeholder": "选对",
            "required": 0,
            "optional": 4
          },
          "text": "disable"
        },
        "tr": {
          "img": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          }
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "43s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": false,
        "bigQuestionEnd": true,
        "smallQuestionStart": true,
        "smallQuestionEnd": true,
        "trCard": true,
        "needRename": true
      }
    },
    {
      "name": "a2p_drag1",
      "type": "2pSet",
      "supportMultiSmallQuestion": false,
      "screen": "show",
      "background": "disable",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": {
            "placeholder": "",
            "required": 1,
            "optional": 5
          },
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": {
            "placeholder": "",
            "required": 1,
            "optional": 3
          },
          "text": "disable"
        },
        "tr": {
          "img": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          }
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "32s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": false,
        "bigQuestionEnd": false,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": true,
        "needRename": false
      }
    },
    {
      "name": "a2p_drag2",
      "type": "2pSet",
      "supportMultiSmallQuestion": true,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": {
            "placeholder": "",
            "required": 0,
            "optional": 6
          },
          "imgEnd": {
            "placeholder": "选对",
            "required": 0,
            "optional": 6
          },
          "videoEnd": "disable",
          "text": "disable",
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": {
            "placeholder": "",
            "required": 1,
            "optional": 5
          },
          "imgEnd": {
            "placeholder": "选对",
            "required": 0,
            "optional": 6
          },
          "text": "disable",
          "offset": {
            "placeholder": "",
            "required": 1,
            "optional": 5
          },
          "disappear": {
            "placeholder": "选对",
            "required": 0,
            "optional": 6
          }
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          }
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "69s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": true,
        "smallQuestionStart": false,
        "smallQuestionEnd": true,
        "trCard": true,
        "needRename": true
      }
    },
    {
      "name": "a2p_drag3",
      "type": "2pSet",
      "supportMultiSmallQuestion": true,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": {
            "placeholder": "",
            "required": 0,
            "optional": 6
          },
          "imgEnd": {
            "placeholder": "选对",
            "required": 0,
            "optional": 6
          },
          "videoEnd": "disable",
          "text": "disable",
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": {
            "placeholder": "",
            "required": 1,
            "optional": 5
          },
          "imgEnd": {
            "placeholder": "选对",
            "required": 0,
            "optional": 6
          },
          "text": "disable",
          "offset": {
            "placeholder": "",
            "required": 1,
            "optional": 5
          },
          "disappear": {
            "placeholder": "选对",
            "required": 0,
            "optional": 6
          }
        },
        "wrong": {
          "imgStart": {
            "placeholder": "",
            "required": 0,
            "optional": 4
          },
          "text": "disable"
        },
        "tr": {
          "img": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          }
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "61s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": false,
        "bigQuestionEnd": true,
        "smallQuestionStart": true,
        "smallQuestionEnd": true,
        "trCard": true,
        "needRename": true
      }
    },
    {
      "name": "a2p_tool",
      "type": "2pSet",
      "supportMultiSmallQuestion": false,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": {
            "placeholder": "",
            "required": 1,
            "optional": 0
          },
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": {
            "placeholder": "",
            "required": 1,
            "optional": 5
          },
          "imgEnd": {
            "placeholder": "选对",
            "required": 0,
            "optional": 6
          },
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": {
            "placeholder": "",
            "required": 0,
            "optional": 4
          },
          "text": "disable"
        },
        "tr": {
          "img": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          }
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "118s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": true,
        "smallQuestionStart": false,
        "smallQuestionEnd": true,
        "trCard": true,
        "needRename": true
      }
    },
    {
      "name": "a2p_reveal",
      "type": "2pSet",
      "supportMultiSmallQuestion": false,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": {
            "placeholder": "下面的图",
            "required": 1,
            "optional": 5
          },
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": {
            "placeholder": "上面的遮盖物",
            "required": 1,
            "optional": 5
          },
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          }
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "13.5s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": true,
        "smallQuestionStart": false,
        "smallQuestionEnd": true,
        "trCard": true,
        "needRename": true
      }
    },
    {
      "name": "a2p_puzzle",
      "type": "2pSet",
      "supportMultiSmallQuestion": false,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": {
            "placeholder": "",
            "required": 2,
            "optional": 6
          },
          "imgEnd": {
            "placeholder": "选对",
            "required": 2,
            "optional": 6
          },
          "text": "disable",
          "offset": {
            "placeholder": "",
            "required": 2,
            "optional": 6
          },
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          }
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "72s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": true,
        "smallQuestionStart": false,
        "smallQuestionEnd": true,
        "trCard": true,
        "needRename": true
      }
    },
    {
      "name": "a2a_free",
      "type": "freeSet",
      "supportMultiSmallQuestion": false,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": {
            "placeholder": "支持多个变种",
            "required": 2,
            "optional": 3
          },
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "12s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "tr_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "tr_2",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "tr_3",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "tr_4",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "tr_5",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": false,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": false,
        "needRename": false
      }
    },
    {
      "name": "pa2a_free",
      "type": "freeSet",
      "supportMultiSmallQuestion": false,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": {
            "placeholder": "",
            "required": 2,
            "optional": 3
          },
          "imgEnd": {
            "placeholder": "读对",
            "required": 2,
            "optional": 3
          },
          "videoEnd": "disable",
          "text": "disable",
          "keywords": {
            "placeholder": "支持多个变种",
            "required": 2,
            "optional": 3
          },
          "offset": {
            "placeholder": "",
            "required": 2,
            "optional": 3
          },
          "disappear": "disable"
        },
        "right": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "41.5s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "tr_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "tr_2",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "tr_3",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "tr_4",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "tr_5",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": true,
        "smallQuestionStart": false,
        "smallQuestionEnd": true,
        "trCard": false,
        "needRename": true
      }
    },
    {
      "name": "a2p_free",
      "type": "freeSet",
      "supportMultiSmallQuestion": false,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "show",
      "srEnd": "show",
      "bigTr": "show",
      "smallQuestion": {
        "stem": {
          "imgStart": {
            "placeholder": "工具",
            "required": 0,
            "optional": 1
          },
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": {
            "placeholder": "",
            "required": 2,
            "optional": 3
          },
          "imgEnd": {
            "placeholder": "选对",
            "required": 2,
            "optional": 3
          },
          "text": "disable",
          "offset": {
            "placeholder": "",
            "required": 0,
            "optional": 6
          },
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          }
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "30s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "tr_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "tr_2",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "tr_3",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "tr_4",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          },
          {
            "name": "tr_5",
            "lines": "optional",
            "actions": "optional",
            "stage": "optional",
            "sound": "optional",
            "special": "optional"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": true,
        "smallQuestionStart": false,
        "smallQuestionEnd": true,
        "trCard": true,
        "needRename": true
      }
    },
    {
      "name": "pa2a_jet1",
      "type": "gameSet",
      "supportMultiSmallQuestion": true,
      "screen": "show",
      "background": "disable",
      "video": "disable",
      "srStart": "disable",
      "srEnd": "disable",
      "bigTr": "disable",
      "smallQuestion": {
        "stem": {
          "imgStart": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          },
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          },
          "keywords": {
            "placeholder": "",
            "required": 1,
            "optional": 0
          },
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "267s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": false,
        "bigQuestionEnd": false,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": false,
        "needRename": false
      }
    },
    {
      "name": "pa2a_jet2",
      "type": "gameSet",
      "supportMultiSmallQuestion": true,
      "screen": "show",
      "background": "disable",
      "video": "disable",
      "srStart": "disable",
      "srEnd": "disable",
      "bigTr": "disable",
      "smallQuestion": {
        "stem": {
          "imgStart": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          },
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          },
          "keywords": {
            "placeholder": "",
            "required": 1,
            "optional": 0
          },
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "267s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": false,
        "bigQuestionEnd": false,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": false,
        "needRename": false
      }
    },
    {
      "name": "pa2a_roleplay1",
      "type": "gameSet",
      "supportMultiSmallQuestion": true,
      "screen": "show",
      "background": "disable",
      "video": "disable",
      "srStart": "disable",
      "srEnd": "disable",
      "bigTr": "disable",
      "smallQuestion": {
        "stem": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": {
            "placeholder": "",
            "required": 1,
            "optional": 0
          },
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "88s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": true,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": false,
        "needRename": true
      }
    },
    {
      "name": "pa2a_roleplay2",
      "type": "gameSet",
      "supportMultiSmallQuestion": true,
      "screen": "show",
      "background": "disable",
      "video": "disable",
      "srStart": "disable",
      "srEnd": "disable",
      "bigTr": "disable",
      "smallQuestion": {
        "stem": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": {
            "placeholder": "",
            "required": 1,
            "optional": 0
          },
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": "disable",
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "88s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": true,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": false,
        "needRename": true
      }
    },
    {
      "name": "a2p_fall",
      "type": "gameSet",
      "supportMultiSmallQuestion": true,
      "screen": "show",
      "background": "show",
      "video": "disable",
      "srStart": "disable",
      "srEnd": "disable",
      "bigTr": "disable",
      "smallQuestion": {
        "stem": {
          "imgStart": {
            "placeholder": "篮子",
            "required": 1,
            "optional": 0
          },
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          },
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": {
            "placeholder": "",
            "required": 1,
            "optional": 5
          },
          "imgEnd": "disable",
          "text": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": {
            "placeholder": "",
            "required": 0,
            "optional": 4
          },
          "text": "disable"
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "65s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": true,
        "bigQuestionEnd": false,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": false,
        "needRename": false
      }
    },
    {
      "name": "a2p_mole",
      "type": "gameSet",
      "supportMultiSmallQuestion": true,
      "screen": "show",
      "background": "disable",
      "video": "disable",
      "srStart": "disable",
      "srEnd": "disable",
      "bigTr": "disable",
      "smallQuestion": {
        "stem": {
          "imgStart": "disable",
          "imgEnd": "disable",
          "videoEnd": "disable",
          "text": "disable",
          "keywords": "disable",
          "offset": "disable",
          "disappear": "disable"
        },
        "right": {
          "imgStart": {
            "placeholder": "",
            "required": 0,
            "optional": 6
          },
          "imgEnd": "disable",
          "text": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          },
          "offset": "disable",
          "disappear": "disable"
        },
        "wrong": {
          "imgStart": {
            "placeholder": "",
            "required": 0,
            "optional": 4
          },
          "text": {
            "placeholder": "",
            "required": 0,
            "optional": 1
          }
        },
        "tr": {
          "img": "disable"
        },
        "videoClip": [
          {
            "name": "prompt_1",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "waiting_1",
            "lines": {
              "placeholder": "150s",
              "type": "disabled"
            },
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "good_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "ok_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          },
          {
            "name": "bad_tr",
            "lines": "required",
            "actions": "required",
            "stage": "required",
            "sound": "required",
            "special": "required"
          }
        ]
      },
      "artboard": {
        "bigQuestionStart": false,
        "bigQuestionEnd": false,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": false,
        "needRename": false
      }
    },
    {
      "name": "t2p_mole",
      "type": "gameSet",
      "supportMultiSmallQuestion": true,
      "screen": "show",
      "background": "disable",
      "video": "disable",
      "srStart": "disable",
      "srEnd": "disable",
      "bigTr": "disable",
      "artboard": {
        "bigQuestionStart": false,
        "bigQuestionEnd": false,
        "smallQuestionStart": false,
        "smallQuestionEnd": false,
        "trCard": false,
        "needRename": false
      }
    }
  ]
}
