const bigQuestionList = []
const smallQuestionList = []
const artboardList = []
const videoClipList = []

console.log('questions.questions', questions.questions)

questions.questions.map(question => {
  let { name, smallQuestion, artboard, ...bigQuestionRest } = question
  if (!smallQuestion) smallQuestion = {}
  const { videoClip, ...smallQuestionRest } = smallQuestion
  bigQuestionList.push({ name, ...bigQuestionRest })
  smallQuestionList.push({ name, ...smallQuestionRest })
  artboardList.push({ name, ...artboard })
  videoClipList.push({ name, videoClip })
})

console.log('bigQuestionList', bigQuestionList)
console.log(JSON.stringify(bigQuestionList, null, 2));
console.log('smallQuestionList', smallQuestionList)
console.log(JSON.stringify(smallQuestionList, null, 2));
console.log('artboardList', artboardList)
console.log(JSON.stringify(artboardList, null, 2));
console.log('videoClipList', videoClipList)
console.log(JSON.stringify(videoClipList, null, 2));
