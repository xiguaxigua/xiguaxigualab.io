function deepCopy (source, parent = null, map = new WeakMap()) {
  const result = {}
  let _parent = parent
  const exist = map.get(source)
  if (exist) return exist
  while (_parent) {
    if (_parent.original === source) return _parent.current 
    _parent = _parent.parent
  }
  map.set(source, result)
  Object.keys(source).forEach(key => {
    const item = source[key]
    if (typeof item === 'object') {
      result[key] = deepCopy(item, {
        original: source,
        current: result,
        parent: parent
      }, map)
    } else {
      result[key] = item
    }
  })
  return result
}

const obj = {
  a: 1,
  b: {
    c: 2
  }
}

obj.d = obj
obj.e = obj.b

const result = deepCopy(obj)
console.log('result', result)
console.log('compare', obj.b === result.b)
console.log('compare1', obj.e === obj.b)
console.log('compare2', result.e === result.b)