import { Imager } from 'mercury-ui'
import React, { Component } from 'react'
import ReactDOM from 'react-dom'

class Comp extends Component {
  constructor (props) {
    super(props)
    console.log(JSON.parse(JSON.stringify(this.props)));
  }
  componentWillMount () {
    console.log('will mount', JSON.parse(JSON.stringify(this.props)))
  }
  componentDidMount () {
    console.log('did mount', JSON.parse(JSON.stringify(this.props)))
  }
  render () {
    return <div>1</div>
  }
}

class App extends Component {
  render () {
    return <div>
      <Comp />
      <Imager />
    </div>
  }
}

ReactDOM.render(<App />, document.getElementById('app'))

