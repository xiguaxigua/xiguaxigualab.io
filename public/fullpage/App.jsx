import classnames from 'classnames';
import Harmmer from 'hammerjs';
import React, { Component } from 'react';

const MAX_NUMBER = 4

export default class App extends Component {
  constructor (props) {
    super(props)
    this.state = { current: 1 }
  }
  componentDidMount () {
    const harmmer = new Harmmer(this.appEl)
    harmmer.get('swipe').set({ direction: Harmmer.DIRECTION_VERTICAL });
    harmmer.on('swipeup', _ => {
      const { current } = this.state
      if (current >= MAX_NUMBER) return
      this.setState({ current: current + 1 })
    })
    harmmer.on('swipedown', _ => {
      const { current } = this.state
      if (current <= 1) return
      this.setState({ current: current - 1 })
    })
  }
  render() {
    const { current } = this.state
    const classname = classnames('slide-container', {
      [`page-${current}`]: true
    })
    const slideClassList = (new Array(4)).fill().map((_, index) => classnames('slide-item', {
      'active': index === current - 1
    }))
    return (
      <div className="app" ref={appEl => this.appEl = appEl}>
        <div className={classname}>
          <div className={slideClassList[0]}>
            <span>1</span>
          </div>
          <div className={slideClassList[1]}>
            <span>2</span>
          </div>
          <div className={slideClassList[2]}>
            <span>3</span>
          </div>
          <div className={slideClassList[3]}>
            <span>4</span>
          </div>
        </div>
      </div>
    );
  }
}
