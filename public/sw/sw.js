const CACHE_NAME = 'my-site-cache-v6';

const urlsToCache = ['/', '/style.css', '/index.js'];

self.addEventListener('install', function(event) {
  console.log('install');
  event.waitUntil(
    caches.open(CACHE_NAME).then(function(cache) {
      console.log('opened cache');
      return cache.addAll(urlsToCache);
    })
  );
});

self.addEventListener('fetch', function(event) {
  console.log('fetch');
  event.respondWith(
    caches.match(event.request).then(function(response) {
      if (response) return response;

      const fetchRequest = event.request.clone();

      return fetch(fetchRequest).then(function(response) {
        if (!response || response.status !== 200 || response.type !== 'basic')
          return response;
        const responseToCache = response.clone();
        caches.open(CACHE_NAME).then(function(cache) {
          cache.put(event.request, responseToCache);
        });

        return response;
      });
    })
  );
});

self.addEventListener('activate', function(event) {
  var cacheWhiteList = ['my-site-cache-v6']
  console.log('activate')
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      console.log('cacheNames', cacheNames)
      return Promise.all(
        cacheNames.map(function(cacheName) {
          if (!~cacheWhiteList.indexOf(cacheName)) return caches.delete(cacheName)
        })
      )
    })
  )
})

self.addEventListener('message', function (event) {
  console.log('on message:', event.data)
  event.source.postMessage('this message is from sw.js')
})