const html = require('htmlparser')
function getHTML (str) {
    const handler = new html.DefaultHandler();
    const parser = new html.Parser(handler);
    parser.parseComplete(str);
    return handler.dom
}

const btn = document.getElementById('btn')
const txr = document.getElementById('txr')
const text = document.getElementById('text')

btn.addEventListener('click', function() {
    text.innerHTML = JSON.stringify(getHTML(txr.value), null, 2)
})
