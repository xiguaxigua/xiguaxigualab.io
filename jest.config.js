module.exports = {
  setupFiles: ['./test/setup.js'],

  transform: {
    '\\.jsx?$': 'babel-jest',
  },

  globals: {
    // __APP_ENV__: 'development',
  },

  // collectCoverage: true,

  testMatch: ['**/*.spec.js?(x)'],

  // collectCoverageFrom: ['src/**/*.{js,jsx}'],

  // https://github.com/ReactTraining/react-router/issues/5030
  testURL: 'http://localhost',
};
